package Views;

import Controllers.ViewController;

import javax.swing.*;
import domain.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ShowPanel extends JPanel {



    JPanel pan1 = new JPanel();
    JButton button = new JButton("BACK");

    public ShowPanel(ViewController controller) {

        int nbEmployees = Integer.valueOf((controller.pers.getNbEmploye()));
        Object[][] data = new Object[nbEmployees+1][];

        ArrayList<Employee> employeesArray = controller.pers.getEmployees();
        int salaireTotal = 0;
        int i = 0;
        for (Employee e : employeesArray) {
            data[i] = (new Object[]{
                    e.getPosition(),
                    e.getName(),
                    String.valueOf(e.getAge()),
                    e.getEntryYear(),
                    e.calculerSalaire()
            });
            salaireTotal += e.calculerSalaire();
            i++;

        }

        data[i]= (new Object[]{"Salaire Moyen","","","",controller.pers.salaireMoyen()});
        String title[] = {"Type", "ID", "Age", "Entry year", "Salary" };
        JTable table = new JTable(data, title);

        JPanel pan2 = new JPanel();

        pan2.add(new JLabel("Total of all salaries : "));
        pan2.add(new JTextField(String.valueOf(salaireTotal)));

        pan1.setLayout(new BoxLayout(pan1, BoxLayout.PAGE_AXIS));
        pan1.add(new JScrollPane(table));
        pan1.add(button);
        add(pan1);

        button.addActionListener(controller);

    }
}
