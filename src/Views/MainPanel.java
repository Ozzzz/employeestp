package Views;

import Controllers.ViewController;

import javax.swing.*;

public class MainPanel extends JPanel {

    JPanel panMain = new JPanel();
    JPanel pan1 = new JPanel();
    JPanel pan2 = new JPanel();
    JButton button1 = new JButton("Add employee");
    JButton button2 = new JButton("Display all employees");

    public MainPanel(ViewController controller) {
        panMain.setLayout(new BoxLayout(panMain, BoxLayout.PAGE_AXIS));
        pan1.setLayout(new BoxLayout(pan1, BoxLayout.LINE_AXIS));
        pan2.setLayout(new BoxLayout(pan2, BoxLayout.LINE_AXIS));
        panMain.add(pan1);
        panMain.add(pan2);
        pan1.add(button1);
        pan2.add(button2);
        add(panMain);

        button1.addActionListener(controller);
        button2.addActionListener(controller);

    }
}


