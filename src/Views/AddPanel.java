package Views;

import Controllers.ViewController;
import domain.*;


import javax.swing.*;
import java.awt.*;

public class AddPanel extends JPanel {

    JPanel pan = new JPanel();
    JPanel pan1 = new JPanel();
    JPanel pan2 = new JPanel();
    JPanel pan3 = new JPanel();
    JPanel pan4 = new JPanel();
    JPanel pan5 = new JPanel();
    JPanel pan6 = new JPanel();
    JPanel pan7 = new JPanel();

    public JComboBox<String> comboBox;
    public JTextField txt_name;
    public JTextField txt_surname;
    public JTextField int_age;
    public JTextField txt_date;
    public JTextField int_salary;
    public String select_type;
    public String[] typeWork = {"Vendeur", "Representant", "Technicien", "Manutentionnaire", "TechARisque", "ManutARisque"};


    public AddPanel(ViewController controller) {

        JButton button1 = new JButton("BACK");
        JButton button2 = new JButton("Submit");
        JLabel name = new JLabel("Name : ");
        JLabel surname = new JLabel("Surname : ");
        JLabel age = new JLabel("Age : ");
        JLabel entry_year = new JLabel("Entry year : ");
        JLabel salary = new JLabel("Salary : ");
        txt_name = new JTextField("", 30);
        txt_surname = new JTextField("", 30);
        int_age = new JTextField("", 30);
        txt_date = new JTextField("", 30);
        int_salary = new JTextField("", 30);
        JLabel type = new JLabel("Type : ");

        JComboBox<String> comboBox = new JComboBox<>(typeWork);
        comboBox.addActionListener(e -> {
            select_type = (String) ((JComboBox<String>) e.getSource()).getModel().getSelectedItem();
        });
        comboBox.setSelectedIndex(0);

        pan.setLayout(new BoxLayout(pan, BoxLayout.PAGE_AXIS));
        pan1.setLayout(new BoxLayout(pan1, BoxLayout.LINE_AXIS));
        pan1.add(name);
        pan1.add(txt_name);
        pan.add(pan1);
        pan2.setLayout(new BoxLayout(pan2, BoxLayout.LINE_AXIS));
        pan2.add(surname);
        pan2.add(txt_surname);
        pan.add(pan2);
        pan3.setLayout(new BoxLayout(pan3, BoxLayout.LINE_AXIS));
        pan3.add(age);
        pan3.add(int_age);
        pan.add(pan3);
        pan4.setLayout(new BoxLayout(pan4, BoxLayout.LINE_AXIS));
        pan4.add(entry_year);
        pan4.add(txt_date);
        pan.add(pan4);
        pan5.setLayout(new BoxLayout(pan5, BoxLayout.LINE_AXIS));
        pan5.add(salary);
        pan5.add(int_salary);
        pan.add(pan5);
        pan6.setLayout(new BoxLayout(pan6, BoxLayout.LINE_AXIS));
        pan6.add(type);
        pan6.add(comboBox);
        pan.add(pan6);
        pan7.setLayout(new FlowLayout());
        pan7.add(button1);
        pan7.add(button2);
        pan.add(pan7);
        add(pan);


        button1.addActionListener(controller);
        button2.addActionListener(controller);

    }


    public Employee addEmployee(){

        Employee e = null;

        String surname =  txt_surname.getText();
        String name = txt_name.getText();
        int age = Integer.parseInt(int_age.getText());
        String entry_year = txt_date.getText();
        int salary = Integer.parseInt(int_salary.getText());

        switch (select_type) {
            case ("Manutentionnaire"):
                e = new Manutentionnaire(surname,name,age,entry_year,salary);
                break;
            case ("ManutARisque"):
                e = new ManutARisque(surname,name,age,entry_year,salary);
                break;
            case ("Technicien"):
                e = new Technicien(surname,name,age,entry_year,salary);
                break;
            case ("TechARisque"):
                e = new TechnARisque(surname,name,age,entry_year,salary);
                break;
            case ("Representant"):
                e = new Representant(surname,name,age,entry_year,salary);
                break;
            case ("Vendeur"):
                e = new Vendeur(surname, name,age,entry_year,salary);
                break;
        }
        return e;

    }
}
