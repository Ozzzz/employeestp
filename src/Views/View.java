package Views;

import Controllers.ViewController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame {

    public JPanel panel;
    public ViewController controller;

    public View(){

        this.setTitle("TP Employees");
        this.controller = new ViewController(this);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new MainPanel(this.controller);

        this.getContentPane().add(new MainPanel(controller));

        this.setSize(600,600);
        this.setVisible(true);
    }


}
