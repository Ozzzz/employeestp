package Controllers;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import domain.*;

import Views.*;
import service.Personnel;

import static java.lang.Integer.parseInt;

public class ViewController implements ActionListener {

    private View view;
    public Personnel pers;

    public ViewController(View view){
        this.pers = new Personnel();
        this.view = view;
    };

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton getThingClicked = (JButton)e.getSource();
        String textOnButton = getThingClicked.getText();



        switch (textOnButton) {
            case "Add employee":
                view.getContentPane().removeAll();
                view.getContentPane().add(new AddPanel(this));
                view.revalidate();
                view.repaint();
                break;
            case "Display all employees":
                view.getContentPane().removeAll();
                view.getContentPane().add(new ShowPanel(this));
                view.revalidate();
                view.repaint();
                break;
            case "BACK":
                view.getContentPane().removeAll();
                view.getContentPane().add(new MainPanel(this));
                view.revalidate();
                view.repaint();
                break;
            case "Submit":
                Component component = view.getContentPane().getComponent(0);
                if(component instanceof AddPanel) {
                    Employee employee = ((AddPanel) component).addEmployee();
                    pers.ajouterEmploye(employee);
                }
                view.getContentPane().removeAll();
                view.getContentPane().add(new MainPanel(this));
                view.revalidate();
                view.repaint();
                break;
        }
    }


}
